package com.home.concurrency;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LoadingCache<K, V> {

	private final Map<K, V> requestmap;
	private final Map<K, V> responseMap;

	public LoadingCache() {
		requestmap = new ConcurrentHashMap<>();
		responseMap = new ConcurrentHashMap<>();
	}

	public V getValueForKey(K key) {
		V value = null;
		if (requestmap.putIfAbsent(key, (V) new LockObject()) == null) {
			// System.err.println(Thread.currentThread() + "--" + requestmap.get(key));
			fetchResponseForKey(key);
		} else {
			if (responseMap.get(key) != null) {
				return responseMap.get(key);
			}
			LockObject lockObject = (LockObject) requestmap.get(key);
			// System.err.println(Thread.currentThread() + "Else" + lockObject);
			synchronized (lockObject) {
				try {
					// System.err.println("Thread" + Thread.currentThread() + "will wait for key " +
					// key);
					lockObject.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		// System.err.println(responseMap);
		// System.out.println(key + "" + responseMap.get(key));
		return responseMap.get(key);

	}

	private void fetchResponseForKey(K key) {
		if (responseMap.get(key) != null) {
			requestmap.remove(key);
			return;
		}
		System.err.println("Fetching Data for key ----" + key + "-Once In Thread" + Thread.currentThread());
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String value = key + "value";
		responseMap.put(key, (V) value);
		synchronized (requestmap.get(key)) {
			requestmap.remove(key).notifyAll();
		}

	}

	private static class LockObject {

		public LockObject() {

		}
	}

}
